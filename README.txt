# Based on http://blog.smalleycreative.com/tutorials/setup-a-django-vm-with-vagrant-virtualbox-and-chef/



# Create a place for the Chef cookbooks
mkdir ~/django_guide/cookbooks

# Change into the newly created ~/django_guide/cookbooks directory
cd ~/django_guide/cookbooks

# Clone the Chef cookbooks repositories as needed (we will use the following cookbooks in this guide)
git clone git://github.com/opscode-cookbooks/apache2.git
git clone git://github.com/opscode-cookbooks/apt.git
git clone git://github.com/opscode-cookbooks/build-essential.git
git clone git://github.com/opscode-cookbooks/git.git
git clone git://github.com/opscode-cookbooks/vim.git


cd build-essentials
git checkout tags/v1.4.4




# get into the vm

vagrant ssh djangovm

# make a new django project

django-admin.py startproject <projectname>

cd <projectname>
python manage.py runserver [::]:8000